from django_mdat_customer.django_mdat_customer.models import *


class DirectoryBackendSettings(models.Model):
    name = models.CharField(max_length=60, unique=True)
    value = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "directory_backend_settings"


class DirectoryTenantSettings(models.Model):
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING, unique=True)
    org_tag = models.CharField(
        max_length=5, default=None, null=True, blank=False, unique=True
    )
    super_admin = models.CharField(
        max_length=200, default=None, null=True, blank=False, unique=True
    )

    class Meta:
        db_table = "directory_tenant_settings"
